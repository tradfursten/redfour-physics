defmodule Calcs do
  def to_nearest_tenth(val) do
    Float.ceil val, 1
  end

  def to_km(velocity) do
    velocity / 1000
  end

  def to_m(km) do
    km *1000
  end

  def squared(x) do
    x * x
  end

  def cubed(x) do
    x * x * x
  end

  def seconds_to_hours(sec) do
    sec / 3600 |> to_nearest_tenth
  end

end

