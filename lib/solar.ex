defmodule Solar do


  def power(%{classification: :X, scale: scale}), do: 1000 * scale
  def power(%{classification: :M, scale: scale}), do: 10* scale
  def power(%{classification: :C, scale: scale}), do: 1* scale

  def no_eva(flares) do
    Enum.filter flares, fn(flare) ->
      power(flare) > 1000
    end
  end

end
