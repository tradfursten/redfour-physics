defmodule PhysicsRocketryTest do
  use ExUnit.Case
  doctest Physics.Rocketry

  test "Escape velocity mars" do
    assert Physics.Rocketry.escape_velocity(:mars) == 5.1
  end

  test "Escape velocity moon" do
    assert Physics.Rocketry.escape_velocity(:moon) == 2.4
  end

  test "Escape velocity earth" do
    assert Physics.Rocketry.escape_velocity(:earth) == 11.2
  end

  test "Orbital acceleration 100 km up" do
    assert Float.round(Physics.Rocketry.orbital_acceleration(100), 2) == 9.51
  end
end
